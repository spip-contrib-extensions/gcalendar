<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'gcalendar_description' => 'Le plugin Gcalendar permet d\'interfacer un site SPIP avec un agenda Google',
	'gcalendar_nom' => 'Gcalendar',
	'gcalendar_slogan' => 'Utiliser un calendrier Google',
);

?>